import sys

sys.path.append("..")

from Database.users import Users
from valid_input import valid_input_user, string_input, valid_name,  valid_mobile_number, valid_username, valid_password

class SignIn:

    def __init__(self):
       self.isValid = False


    def valid_input(self, user_input):
       if(user_input.isnumeric()):
       	   return True
       else:
            print("\n {0:*^50s}  \n ".format(" Please Enter valid Input  "))
            return False


    def user_sign_in(self):
        print("\n {0:*^50s}  \n ".format(" Login Account "))

        username = valid_username()
        password = valid_password()
        c = Users()
        is_matched, value = c.matchPassword(username, password)
        if(is_matched):
          print("\nSuccessfull Login Account \n")
          return True, username
        else:
           print("\nWrong username and password ")   

            
        return self.isValid, None         


if __name__ == "__main__":
    s = SignIn()
    print(s.user_sign_in())

        