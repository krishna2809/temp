import sys

sys.path.append("..")

from Database.database_connection import DatabaseConnection
from valid_input import valid_input_user, string_input, valid_name,  valid_mobile_number
from query import * 



class Driver:


    def __init__(self):
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()




    def add_driver(self):

            driving_licence, is_valid = string_input("\nEnter Driver licence : ")

            mycursor =  self.isConnected.cursor()
            cursor = self.isConnected.cursor()
            cursor.execute(is_true_driver, (driving_licence, ))
            # it is return list of data 
            rows=cursor.fetchall()

            if(len(rows) != 0):
                print("\ndriver is already exist")
                return  
            name = valid_name("\nEnter Name : ")
            mobile_number = valid_mobile_number()
            city = valid_name("\nEnter City : ")
            state = valid_name("\nEnter State : ")
            
            mycursor =  self.isConnected.cursor()
        
            driver_data = (driving_licence , name, mobile_number, "YES", state, city)
            mycursor.execute(driver_details_insert, driver_data)
            self.isConnected.commit()
            print("\nDriver is added successfully ")
            return True 
       


    def is_driver_available(self, driver_licence):

        cursor = self.isConnected.cursor()
        cursor.execute(select_driver_licence, (driver_licence, ))
        # if query is right then return rows of data in list formate 
        records = cursor.fetchall()
        if(len(records) != 0):
            return True 

        print("\nWrong driving Licence Number ")
        return False     


    
    def update_driver(self):
          driver_licence, is_valid = string_input("\nEnter Driver Licence : ")
          # driver_licence = 'GU-1234567890123'
          if(self.is_driver_available(driver_licence)):
              mobile_number = valid_mobile_number() 
              cursor = self.isConnected.cursor()
              cursor.execute(mobile_number_select_driver, (mobile_number, ))
              record =  cursor.fetchone()
              # print("mobile number : ", recorde)
              if(len(record) == 0 ):
                # if query is right then return rows of data in list formate 
                  cur = self.isConnected.cursor()
                  cur.execute(mobile_number_update_driver, (mobile_number, driver_licence))
                  self.isConnected.commit()
                  print("\nDriver's Mobile Number is updated ")
                  return True
              else:
                  print("\nMobile Number is already exist")   
           
          return False    

    def delete_driver(self):
          print("Show Driver Details ")
          if(self.show_driver_details()):
              driver_licence, is_valid = string_input("\nEnter Driver Licence : ")
              # driver_licence = 'GU-1234567890123'
              if(self.is_driver_available(driver_licence)):
                  cur = self.isConnected.cursor()
                  cur.execute(delete_driver, (driver_licence, ))
                  self.isConnected.commit()
                  print("\nSuccessfully deleted")
                  return True
              return False  


    def delete_book_driver(self):
        print("Show Booked Driver ")
        if(self.show_booked_driver()):
            driver_licence, is_valid = string_input("\nEnter Driver Licence : ")

            # sql = "SELECT * FROM driver_book WHERE driver_licence = %s"
            cursor = self.isConnected.cursor()
            cursor.execute(select_booked_driver, (driver_licence, ))
            # if query is right then return rows of data in list formate 
            records = cursor.fetchall()
            if(len(records) != 0 ):
                  cur = self.isConnected.cursor()
                  cur.execute(delete_booked_driver, (driver_licence, ))
                  self.isConnected.commit()
                  print("\nSuccessfully deleted")
                  return True
            else:
                print("Records is not available")      
            return False  




               

    def show_driver_details(self):

            choice, is_valid = valid_input_user("\n[1] Search Driver  \t [2] Show drivers List \t[3] Back \nEnter Choice : ")

            if(is_valid):
                if(choice == '1'):
                      driver_licence, is_valid = string_input("\nEnter Driver Licence : ")
                      # driver_licence = 'GU-1234567890123'
                      if(self.is_driver_available(driver_licence)):
                        cursor = self.isConnected.cursor()
                        cursor.execute(select_driver_with_licence, (driver_licence, ))
                        records  = cursor.fetchall()
                        if(len(records) != 0 ):
                            for row in records:

                                # print(type(row), len(row))
                                print(f"\nDriving Licence : {row[0]} | \tName : {row[1]} | \tMobile Number : {row[2]} | \tJoining Date : {row[3]} | \tDriver Available : {row[4]} | \tState : {row[5]} |\t City : {row[6]}\n")
                            return True
                        return False        

                elif(choice == '2'):
                    cursor = self.isConnected.cursor()
                    cursor.execute(select_driver)
                    records = cursor.fetchall()
                    if(len(records) != 0):
                          print(" {0:-^50s}  \n ".format(""))
                          for row in records:
                                print(f"\nDriving Licence : {row[0]} | \tName : {row[1]} | \tMobile Number : {row[2]} | \tJoining Date : {row[3]} | \tDriver Available : {row[4]} | \tState : {row[5]} |\t City : {row[6]}\n")
                                print(" {0:-^50s}  \n ".format(""))
                          return True      
                    
                    print("\nDriver records isn't Available")
                    return False
                
                elif(choice == '3'):
                    print("\nThank You")        


                else:
                    print("\nWrong Choice ")        
            return False     
        
    def show_booked_driver(self):
            cursor = self.isConnected.cursor()
            cursor.execute(select_booked_driver)
            records = cursor.fetchall()
            if(len(records) != 0):
                print("\n {0:-^50s}  \n ".format(""))

                for row in records:
                        print(f"Car Number : {row[0]} | \tUser ID  : {row[1]} |\tDriver Licence : {row[2]} |\tDriver Pick Up Car : {row[3]} |\tDriver Drop Off Car : {row[4]}\n")
                        print("\n {0:-^50s}  \n ".format(""))
                return True        

            print("\nDriver Booked records isn't Available")
            return False






    def operations(self):
        if(self.isConnected != None):

            isContinue = True
            while isContinue:
                choice, is_valid_input = valid_input_user("\n[1] Add driver \t [2] Update Driver \t[3] Delete Driver  \t[4] Delete Book Driver \t[5] Show Driver Details\t [6] Show Booked Drivers [7] Back\nEnter Choice : ")
                if(is_valid_input):
                    if(choice == '1'):
                       self.add_driver()
                    elif(choice == '2'):
                        self.update_driver()
                    elif(choice == '3'):
                        self.delete_driver()
                    elif(choice == '4'):
                         self.delete_book_driver()    
                    elif(choice == '5'):
                        self.show_driver_details()
                    elif(choice == '6'):
                        self.show_booked_driver()
                    elif(choice == '7'):
                        isContinue = False    
                        self.isConnected.close()  
                    else: 
                       print("\nWrong Choice")



if __name__ == "__main__":
    driver = Driver()
    driver.operations()               

               	