
# UserDashBoard 
car_rent_select =  "SELECT * FROM car_rent WHERE user_id = %s"
username_select = "SELECT * FROM users WHERE username = %s"
update_licence = "UPDATE  users  SET licence_number = %s WHERE username = %s"

update_state = "UPDATE users SET state = %s WHERE username = %s"
update_city = "UPDATE users SET city = %s WHERE username = %s"

car_details_select = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s"

car_type_id_select = "SELECT * FROM car_type WHERE car_type_id = %s "
car_name_id_select = "SELECT * FROM car_name WHERE car_id = %s"

driver_select = "SELECT * FROM driver WHERE state = %s and city = %s  and is_available = %s "

update_wallet = "UPDATE users  SET wallet = %s WHERE username = %s"
car_rent_insert = "INSERT INTO car_rent (car_number, price,  user_id, is_driver_booked, car_pick_up_date, car_drop_off_date) VALUES (%s, %s, %s, %s, %s, %s)"

car_details_update = "UPDATE car_details  SET is_car_available = %s WHERE car_number = %s"
update_driver_details = "UPDATE driver  SET is_available = %s WHERE driver_licence = %s"
driver_booked_insert = "INSERT INTO driver_book (car_number, user_id,  driver_licence, driver_pick_up_car, driver_drop_off_date) VALUES (%s, %s, %s, %s, %s)"

# user.py

user_insert = 'INSERT INTO users (username , mobile_number, password, is_super_user) VALUES (%s, %s, %s, %s) '
select_user_with_password_username = "select  username ,password from users"
select_user_with_wallet = "select username, wallet from users where username = %s"

# driver.py 
is_true_driver = "SELECT * FROM driver WHERE  EXISTS (SELECT * FROM driver WHERE driver_licence = %s )"
           
driver_details_insert = 'INSERT INTO driver (driver_licence , name, mobile_number, is_available, state, city) values(  %s, %s, %s, %s, %s, %s) '
        
select_driver_licence  = "SELECT driver_licence FROM driver WHERE driver_licence = %s"
    
mobile_number_select_driver = "SELECT mobile_number FROM driver WHERE mobile_number = %s"
                     

mobile_number_update_driver = "UPDATE driver  SET mobile_number = %s WHERE driver_licence = %s"
                         

delete_driver = "DELETE FROM driver WHERE driver_licence = %s"
select_booked_driver =  "SELECT * FROM driver_book WHERE driver_licence = %s"          
delete_booked_driver = "DELETE FROM driver_book WHERE driver_licence = %s"
            
       

select_driver_with_licence = "SELECT * FROM driver WHERE driver_licence = %s"
                       
select_driver = "SELECT * FROM driver"
select_booked_driver = "SELECT * FROM driver_book "

# sign_up
user_exist_select = "select * from users where exists (select * from users where username = %s )"

# cars.py

car_name_select = "SELECT * FROM car_name"

car_details_select = "SELECT * FROM car_details"

car_type_select =  "SELECT * FROM car_type"

car_rent_select = "SELECT * FROM car_rent"

car_type_name_is_true = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_name = %s )"
           

car_type_insert = "INSERT INTO car_type (car_type_name) VALUES (%s)"
              

car_name_is_true = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_name = %s )"


car_type_id_is_true = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"
car_name_insert = "INSERT INTO car_name (car_name, total_car,  four_hour_price, per_day_price, car_type_id) VALUES (%s, %s, %s, %s, %s)"

car_details_is_true = "SELECT * FROM car_details WHERE  EXISTS (SELECT * FROM car_details WHERE car_number = %s )"
                  
car_id_is_true = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_id = %s )"
                 

car_details_insert = "INSERT INTO car_details (car_number, car_color,  state, city, car_id, car_type, is_car_available) VALUES (%s, %s, %s, %s, %s, %s, %s)"
delete_car_type =  "DELETE FROM car_type WHERE car_type_id = %s"
delete_car_name  = "DELETE FROM car_name WHERE car_id = %s"
                
delete_car_detail = "DELETE FROM car_details WHERE car_number = %s"
car_rent_is_true = "SELECT * FROM car_rent WHERE  EXISTS (SELECT * FROM car_rent WHERE car_number = %s )"
delete_car_rent = "DELETE FROM car_rent WHERE car_number = %s"

update_car_type = "UPDATE car_type  SET car_type_name = %s WHERE car_type_id = %s"
               
car_name_select_with_name= "SELECT * FROM car_name WHERE car_name = %s"
update_car_name = "UPDATE car_name  SET car_name= %s WHERE car_id = %s"
update_total_car = "UPDATE car_name  SET total_car = %s WHERE car_id = %s"
update_four_hour_price =  "UPDATE car_name  SET four_hour_price = %s WHERE car_id = %s"

update_per_day_price = "UPDATE car_name  SET per_day_price = %s WHERE car_id = %s"
car_color_update = "UPDATE car_details  SET car_color = %s WHERE car_number = %s"
                                       
car_state_update = "UPDATE car_details SET state = %s WHERE car_number = %s"
car_city_update  = "UPDATE car_details  SET city = %s WHERE car_number = %s"

car_details_with_car_number = "SELECT * FROM car_details WHERE car_number = %s"     

user_history_insert = "INSERT INTO user_history (user_id, is_driver_booked, car_pick_up_date, car_drop_off_date, car_type_name, car_name, price) VALUES (%s, %s, %s, %s, %s, %s, %s)"

show_user_history = "SELECT * FROM user_history WHERE user_id = %s"

car_details_select_filter = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s"

car_details_filter_id = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s and car_id = %s " 


admin_wallet = "SELECT wallet FROM users WHERE username =  %s"
admin_wallet_update = "UPDATE users set wallet = %s WHERE username = %s" 

car_type_is_true = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"

user_select = "SELECT * FROM users WHERE  is_super_user = 'NO' and wallet = '0' and username = %s "
delete_user = "DELETE FROM users WHERE username = %s"

user_details = "SELECT * FROM users"

update_home_town = "UPDATE users SET home_town = %s WHERE username = %s"

update_driving_licence_car_rent = "UPDATE car_rent set driver_licence = %s WHERE car_number = %s"

select_user_licence = "SELECT * FROM users where username = %s"
select_car_rent_driver_licence = "SELECT * FROM car_rent WHERE is_driver_booked = %s AND car_number = %s"


update_driver_is_available = "UPDATE driver set is_available = %s WHERE driver_licence = %s"



 







