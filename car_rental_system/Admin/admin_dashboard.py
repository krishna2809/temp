
import sys

sys.path.append("..")

from Admin.cars import Car
from Admin.drivers import Driver
from valid_input import valid_input_user


class AdminDashBoard:
    def __init__(self, username):
       self.username  = username

    

    def dash_board(self):
        print("\n {0:*^50s}  \n ".format(" Welcome to Car Rental System  Of Admin Interface"))
        isContinue = True
        while(isContinue): 
            choice , is_valid_input =  valid_input_user("\n[1] Cars \t[2] Drivers  \t[3] Quit \nEnter Choice : ")
            if(is_valid_input):
                if(choice == '1'):
                    car = Car()
                    car.operations()
                elif(choice == '2'):
                    driver = Driver()
                    driver.operations()
                elif(choice == '3'):
                   print("\nThank You")
                   isContinue = False
                else: 
                   print("\nWrong Input")   


if __name__ == "__main__":
   pass       		



