
from datetime import date
import sys
sys.path.append("..")
from Database.database_connection import DatabaseConnection
from Database.users import Users
from valid_input import valid_input_user, string_input, valid_name, valid_date_month_year

class UserDashBoard:

    def __init__(self, username):
        self.username = username
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()


    def showHistory(self):
        userId = self.get_userId()
        sql = "SELECT * FROM car_rent WHERE user_id = %s"
        cursor = self.isConnected.cursor()
        cursor.execute(sql, ( userId  ,  ))
        records  = cursor.fetchall()
        if(len(records) !=0 ):
            for row in records:
                print(f"Car Number : {row[0]} | Price : {row[1]} | Username : {self.username} | Driver Booked : {row[3]} | Car Pick Up Date : {row[4]} | Car Drop Off Date : {row[5]}\n")
        else:
            print("History is not available")    



         
    def get_userId(self):
       sql = "SELECT * FROM users WHERE username = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(sql, (self.username, ))
       records = cursor.fetchall()
       # print(records)
       userId = records[0][0]
       # print(userId)
       return userId


    def wallet(self):
            sql = "SELECT * FROM users WHERE username = %s"
            cursor = self.isConnected.cursor()
            cursor.execute(sql, (self.username, ))
            records = cursor.fetchall()
           # print(records)
            getAmount = int(records[0][5])
            # getAmount =  self.records[4]
            print("Your Wallet Amount : ", getAmount)
            choice = "\n[1] Add Amount\t [2] Back\nEnter Choice : "
            choice, is_valid_input =  valid_input_user(choice)
            # choice  = input("\n[1] Add Amount\t [2] Back\nEnter Choice : ")
            if(is_valid_input):
                choice = int(choice)
                if(choice == 1):
                    # self.addAmount()
                    # amount =  input("\nAdd Amount in Wallet : ")
                    amount = "\nAdd Amount in Wallet : "
                    amount, is_valid_input =  valid_input_user(amount)


                    if(is_valid_input):
                        user_obj = Users()
                        isAddAmount =  user_obj.addAmount(self.username, str(int(amount) + getAmount))
                        if(isAddAmount):
                            print("\nAmount is Successfully Add ")
                            self.menu()
                elif(choice == 2):
                     self.menu()
                else:
                    print("\nWrong choice")
                    

    def add_licence(self):
       licence_number, is_valid = string_input("\nEnter Licence Number : ")
       sql = "UPDATE  users  SET licence_number = %s WHERE username = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(sql, (licence_number, self.username))
       self.isConnected.commit()
       print("\nLicence Number is Added")

    def licence_available(self):
        sql = "SELECT * FROM users WHERE username = %s"

        cursor = self.isConnected.cursor()
        cursor.execute(sql, (self.username , ))
        row  = cursor.fetchall()
        if(row[0][8] == None):
            print("Driving Licence is not available so First add Driving Licence ")
            is_added = self.user_licence()
            if(is_added == False):
                self.licence_available()
        return True        



                        
                    
    def user_licence(self):
        sql = "SELECT * FROM users WHERE username = %s"

        cursor = self.isConnected.cursor()
        cursor.execute(sql, (self.username , ))
        row  = cursor.fetchall()

        print("Driving Licence : ", row[0][8])
        choice, is_valid = valid_input_user("\n[1] Add Licence Number \t[2] Back\nEnter Choice : ")
        if(choice == '1'):
            if(row[0][8] == None):
                self.add_licence()
                return True
        elif(choice == '2'): 
            return False
        else:
            return False
            print("Wrong Input")
            # self.user_licence()  

           



    def menu(self):
        choice_sub_menu = "\n[1] Show History\t [2] Wallet \t[3] Licence Number \t[4] Back\nEnter Choice : "
        choice_sub_menu, is_valid_input =  valid_input_user(choice_sub_menu)

        if(is_valid_input):
            choice_sub_menu = int(choice_sub_menu)
            if(choice_sub_menu == 1):
                self.showHistory()
            elif(choice_sub_menu == 2):
                self.wallet()
            elif(choice_sub_menu == 3):
                self.user_licence()    
            elif(choice_sub_menu == 4):
                return  
            else:
                print("\nWrong Number ")



     


    def pick_up_car(self):
 
       current_state  =  valid_name("\nEnter current state name :  ")
       current_city  =  valid_name(" \nEnter current city name : ")
       cursor = self.isConnected.cursor()
       
       sql = "UPDATE users SET state = %s WHERE username = %s"
       cursor.execute(sql, (current_state, self.username ))
       self.isConnected.commit()


       cursor = self.isConnected.cursor()
       sql = "UPDATE users SET city = %s WHERE username = %s"
       cursor.execute(sql, (current_city, self.username))
       self.isConnected.commit()
       print("\nLocation Updated")

       sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(sql, (current_state, current_city, "YES"))
       records = cursor.fetchall()
       # print(records)
       car_id_list, car_type_id_list, choice_car_id  = [], [], []
       if(len(records) != 0):
            for row in records:
                car_id_list.append(row[4])
                car_type_id_list.append(row[5])
                choice_car_id.append(row[4])
            # print(car_id_list)
            # print(car_type_id_list)   
            # print("--------------------------------") 
            for car_type_id in car_type_id_list:
                sql = "SELECT * FROM car_type WHERE car_type_id = %s "
                cursor = self.isConnected.cursor()
                cursor.execute(sql, (car_type_id, ))
                records = cursor.fetchall()
                if(len(records) != 0 ):
                    for row in records:
                        print("--------------------------------") 
                        print("\nCar Type Name : ", row[1])
                        car_id = car_id_list[0]
                        car_id_list.pop(0)
                        sql = "SELECT * FROM car_name WHERE car_id = %s"
                        cursor = self.isConnected.cursor()
                        cursor.execute(sql, (car_id, ))
                        records = cursor.fetchall()
                        if(len(records) !=0):

                            for row in records:
                               print("Car ID : ", car_id)
                               print("Car Name : ", row[1])
                               four_hour_price = row[3]
                               per_day_price = row[4]
                               # print("Four Hour Price : ",four_hour_price)
                               print("Per Day Price : ", per_day_price)



            # select_car_id  = input("\nEnter Car ID : ")
            select_car_id, is_valid  = valid_input_user("\nEnter Car ID : ")
            # print(select_car_id,  choice_car_id)

            if(int(select_car_id) in choice_car_id ):
                per_day_driver_price = 1000
                car_pick_date , date1,  = valid_date_month_year("\nEnter Year and Month and Date  for pick Up car ")
                # car_pick_date = "{0}-{1}-{2} {3}:{4}:".format(pick_date, pick_month, pick_year, pick_time, "00:00")    
                car_drop_off_date, date2 = valid_date_month_year("\nEnter Year and Month and Date  for Drop Off car ")
            
                # car_drop_off_date = "{0}/{1}/{2}-{3}:{4}".format(drop_date, drop_month, drop_year, drop_time, "00:00")
                # car_drop_off_date = "{0}-{1}-{2} {3}:{4}:".format(drop_date, drop_month, drop_year, drop_time, "00:00")

                # print(car_drop_off_date, '2022-10-5 14:00:00-08')

                # '2022-10-5 14:00:00-08'

                different = (date2 - date1).days
                if(different > 0):
                    price = different * per_day_price

                    is_want_driver = input("\nDo you want to driver yes/no ?: ")
                    payment_succesfully, is_driver_book = False, "NO" 
                    amount = per_day_price * different 
                    if(is_want_driver == 'YES' or is_want_driver == 'yes'):


                       sql = "SELECT * FROM driver WHERE state = %s and city = %s  and is_available = %s "
                       cursor = self.isConnected.cursor()
                       cursor.execute(sql, (current_state, current_city, "YES"))
                       records = cursor.fetchall()
                       print(records)
                       if(len(records) !=0):
                            driver_licence = records[0][0]
                            total_driver_amount = per_day_driver_price * different
                            print("Driver's amount is : ", total_driver_amount)
                           
                            price =  amount + total_driver_amount
                            is_driver_book = "YES"
                       else:
                          is_continue = input("Driver is not available do you want to continue of only rent car yes/no? ")
                          if(is_continue == 'YES' or is_continue == 'yes'):
                            price = amount
                            self.licence_available()
                             
                          else:
                              return   

                    elif(is_want_driver == 'NO' or is_want_driver == 'no' or is_want_driver == 'No'):
                        price = amount
                        self.licence_available()
                    else:
                         print("Invalid input")
                         return  
                    is_pay =  input("Do you want to Pay yes/no ? ")
                    if(is_pay == 'yes' or is_pay  == 'YES'):
                        sql = "SELECT * FROM users WHERE username = %s"
                        cursor = self.isConnected.cursor()
                        cursor.execute(sql, (self.username, ))
                        records = cursor.fetchall()
                        # print(records)
                        wallet =  records[0][5]
                        wallet = int(wallet)
                        if( price <= wallet):
                              sql = "UPDATE users  SET wallet = %s WHERE username = %s"
                              cur = self.isConnected.cursor()
                              cur.execute(sql, (int(wallet - price), self.username))
                              self.isConnected.commit()
                              payment_succesfully = True
                        else:
                             print("Add Amount in wallet ")  
                             self.wallet()
                             self.pick_up_car()   



                        if(payment_succesfully):
                           sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s and car_id = %s "
                           cursor = self.isConnected.cursor()
                           cursor.execute(sql, (current_state, current_city, "YES", select_car_id))
                           records = cursor.fetchall()
                           # print(records)
                           car_number = records[0][0]

                           
                           userId = self.get_userId()

                           sql = "INSERT INTO car_rent (car_number, price,  user_id, is_driver_booked, car_pick_up_date, car_drop_off_date) VALUES (%s, %s, %s, %s, %s, %s)"
                           cursor = self.isConnected.cursor()
                           details =  (car_number, price,  userId, is_driver_book, car_pick_date, car_drop_off_date)
                           cursor.execute(sql, details)
                           self.isConnected.commit()


                           print("yes")
                           # return 


                           sql = "UPDATE car_details  SET is_car_available = %s WHERE car_number = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(sql, ("NO", car_number ))
                           self.isConnected.commit()

                           if(is_driver_book == 'YES'):
                               sql = "SELECT * FROM driver WHERE state = %s and city = %s  and is_available = %s "
                               cursor = self.isConnected.cursor()
                               cursor.execute(sql, (current_state, current_city, "YES"))
                               records = cursor.fetchall()

                               # print(records)
                               if(len(records) !=0):
                                   driver_licence = records[0][0]
                                   sql = "UPDATE driver  SET is_available = %s WHERE driver_licence = %s"
                                   cursor = self.isConnected.cursor()
                                   cursor.execute(sql, ("NO", driver_licence ))
                                   self.isConnected.commit()

                                   sql = "INSERT INTO driver_book (car_number, user_id,  driver_licence, driver_pick_up_car, driver_drop_off_date) VALUES (%s, %s, %s, %s, %s)"
                                   cursor = self.isConnected.cursor()
                                   details =  (car_number, userId,  driver_licence, car_pick_date, car_drop_off_date)
                                   cursor.execute(sql, details)
                                   self.isConnected.commit()

                                   
                               else:
                                   print("Driver is not available")    


                           print("\nCar rent Successfully")



                else:
                     print("\nInvalid Date ")    

            else:
                print("\nInvalid select car id ")    
 
       else:
           print("\nCar is not available for current city ")
           return           



    def dash_board(self):
        if(self.isConnected != None):
            choice_interface = "\n[1] Menu\t [2] Pick Up Car \t[3] Log Out \nEnter Choice : "
            choice_interface, is_valid_input =  valid_input_user(choice_interface)
            if(is_valid_input): 
                choice_interface = int(choice_interface)
                if(choice_interface == 1):
                    self.menu()
                    self.dash_board() 
                elif(choice_interface == 2 ):
                    self.pick_up_car()
                    self.dash_board() 
                elif(choice_interface == 3):
                    print("\nSuccessfully LogOut Account \n")   
                    return
                else:
                   print("\nWrong choice ")  
                   self.dash_board()     


if __name__ == '__main__':
    dashboard = UserDashBoard('hello1')
    # dashboard.wallet()                    
    dashboard.dash_board() 

