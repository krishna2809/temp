import random
import sys
sys.path.append("..")
from Database.users import Users
from Database.database_connection import DatabaseConnection
from valid_input import valid_input_user, string_input, valid_name,  valid_mobile_number, valid_password, valid_username





class SignUp:
   def __init__(self):
     self.isValid = False 

   def valid_input(self, user_input):
      if(user_input.isnumeric()):
           return True
      else:
            print("\n {0:*^50s}  \n ".format(" Please Enter valid Input  "))
            return False



   def user_sign_up(self):
      
      database_obj = DatabaseConnection()
      isConnected = database_obj.connecting()
      if(isConnected != None):
         print("\n {0:*^50s}  \n ".format(" SignUp Account "))
         username = valid_username()
         mycursor =  isConnected.cursor()
         sql = "select * from users where exists (select * from users where username = %s )"
         cursor = isConnected.cursor()
         cursor.execute(sql, (username, ))
         # it is return list of data 
         rows=cursor.fetchall()
         if(len(rows) != 0):
            print("\nUser name already exist")
            return self.isValid
         phone_number = valid_mobile_number()
         
         otp = random.randint(1000, 9999)
         print("OTP : ", otp)
         enter_otp, is_valid = valid_input_user("\nEnter OTP : ")
         
         if(otp == int(enter_otp)):
            password =  valid_password()
            print(password)
            c = Users()
            if(c.set_details(username , phone_number, password , "NO")):          

               self.isValid = True 
               return self.isValid
         else:
             print("\nWrong OTP ")
         
      return self.isValid 			


if __name__ == "__main__":           
   s = SignUp()
   s.user_sign_up()
