
import sys, os 
# print("************ ",os.path.abspath(__file__))

# t = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# print(t)  
# sys.path.append(t)

# if __name__ == "__main__":
#     from database_connection import DatabaseConnection
# else:
#     from Database.database_connection import DatabaseConnection

sys.path.append("..")

from Database.database_connection import DatabaseConnection


class Users:

    def __init__(self):
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()

    def set_details(self, username , mobile_number, password, is_super_user):
        # dababase_obj = DatabaseConnection()
        # isConnected = dababase_obj.connecting()
        if(self.isConnected != None):
            mycursor =  self.isConnected.cursor()
            sql =  'INSERT INTO users (username , mobile_number, password, is_super_user) VALUES (%s, %s, %s, %s) '
        
            user_data = (username , mobile_number, password, is_super_user)
            mycursor.execute(sql, user_data)
            self.isConnected.commit()
            print("\nAccount Created")
            return True 
        return False    

    def matchPassword(self, username, password):
        # dababase_obj = DatabaseConnection()
        # isConnected = dababase_obj.connecting()
        if(self.isConnected != None):
            sql = "select  username ,password from users"
            cursor = self.isConnected.cursor()
            cursor.execute(sql)
            records = cursor.fetchall()

            for row in records:
                if(row[0] == username and row[1] == password):
                      # print(row[0], row[1])
                      return True, username

        return False, None     
    def getWallet(self, username):
        # dababase_obj = DatabaseConnection()
        # isConnected = dababase_obj.connecting()
        if(self.isConnected != None):
            sql = "select username, wallet from users where username = %s"
            cursor = self.isConnected.cursor()
            cursor.execute(sql, (username, ))
            records = cursor.fetchall()
            
            for row in records:
                if(row[0] == username): 
                      return int(row[1])
        return False

    
    def addAmount(self, username, amount):

        # dababase_obj = DatabaseConnection()
        # isConnected = dababase_obj.connecting()
        if(self.isConnected != None):
            sql = "update users set wallet = %s where username = %s"
            cur = self.isConnected.cursor()
            cur.execute(sql, (amount, username))
            self.isConnected.commit()
            return True
        return False    


                      
                

if __name__ == "__main__":
    u = Users()
    print(u.matchPassword('krishna', '2222'))
    print(u.getWallet('krishna'))
    print(u.addAmount('krishna', u.getWallet('krishna') + 100))

    print(u.getWallet('krishna'))

    print(u.addAmount('krishna', u.getWallet('krishna') + 100))
