
from datetime import date
import sys
sys.path.append("..")
from Database.database_connection import DatabaseConnection
from Database.users import Users
from valid_input import valid_input_user, string_input, valid_name, valid_date_month_year, take_decision, valid_address
from query import * 

class UserDashBoard:

    def __init__(self, username):
        self.username = username
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()

    def add_admin_wallet(self, price):
       cursor = self.isConnected.cursor()
       cursor.execute(admin_wallet, ('admin123',))
       records = cursor.fetchall()
       if(len(records) != 0 ):
           wallet = int(records[0][0])
           cursor = self.isConnected.cursor()
          
           cursor.execute(update_wallet, (int(wallet + price), 'admin123'))
           self.isConnected.commit()


    
    def showHistory(self):
        userId = self.get_userId()
        # sql = "SELECT * FROM car_rent WHERE user_id = %s"
        cursor = self.isConnected.cursor()
        cursor.execute(show_user_history, (userId,))
        records  = cursor.fetchall()
        if(len(records) !=0 ):
            for row in records:
                print(f"is_driver_book: {row[1]} | Car Rent Date : {row[2]} | Car Drop Off Date : {row[3]} | Car Type Name : {row[4]} | Car Name : {row[5]} \n")
        else:
            print("History is not available")  


    def add_user_history(self, details):

        # details =  (car_number, price,  userId, is_driver_book, car_pick_date, car_drop_off_date)
        cursor = self.isConnected.cursor()
        cursor.execute(car_details_with_car_number, (details[0],))
        records  = cursor.fetchall()
        if(len(records) !=0 ):
            car_color, car_id, car_type_id  = records[0][1], records[0][4], records[0][5] 
            cursor = self.isConnected.cursor()
            cursor.execute(car_name_id_select, (car_id,))
            records  = cursor.fetchall()
            if(len(records) != 0 ):
                car_name = records[0][1]
            cursor = self.isConnected.cursor()
            cursor.execute(car_type_id_is_true, (car_type_id, ))
            records  = cursor.fetchall() 
            if(len(records) != 0 ):
                car_type_name = records[0][1] 
                insert_date = (details[2], details[3], details[4], details[5], car_type_name, car_name, details[1])
                cursor = self.isConnected.cursor()
                cursor.execute(user_history_insert, insert_date)
                self.isConnected.commit()

         
    def get_userId(self):
       # sql = "SELECT * FROM users WHERE username = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(username_select,  (self.username, ))
       records = cursor.fetchall()
       # print(records)
       userId = records[0][0]
       # print(userId)
       return userId


    def wallet(self):
            # sql = "SELECT * FROM users WHERE username = %s"
            cursor = self.isConnected.cursor()
            cursor.execute(username_select, (self.username, ))
            records = cursor.fetchall()
           # print(records)
            getAmount = int(records[0][5])
            # getAmount =  self.records[4]
            print("Your Wallet Amount : ", getAmount)
            choice = "\n[1] Add Amount\t [2] Back\nEnter Choice : "
            choice, is_valid_input =  valid_input_user(choice)
            if(is_valid_input):
                choice = int(choice)
                if(choice == 1):
                    # self.addAmount()
                    # amount =  input("\nAdd Amount in Wallet : ")
                    amount = "\nAdd Amount in Wallet : "
                    amount, is_valid_input =  valid_input_user(amount)


                    if(is_valid_input):
                        user_obj = Users()
                        isAddAmount =  user_obj.addAmount(self.username, str(int(amount) + getAmount))
                        if(isAddAmount):
                            print("\nAmount is Successfully Add ")
                            self.menu()
                elif(choice == 2):
                     self.menu()
                else:
                    print("\nWrong choice")
                    

    def add_licence(self):
       licence_number, is_valid = string_input("\nEnter Licence Number : ")
       # sql = "UPDATE  users  SET licence_number = %s WHERE username = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(update_licence, (licence_number, self.username))
       self.isConnected.commit()
       print("\nLicence Number is Added")

    def licence_available(self):
        # sql = "SELECT * FROM users WHERE username = %s"

        cursor = self.isConnected.cursor()
        cursor.execute(username_select, (self.username , ))
        row  = cursor.fetchall()
        if(row[0][8] == None):
            print("Driving Licence is not available so First add Driving Licence ")
            is_added = self.user_licence()
            if(is_added == False):
                self.licence_available()
        return True        



                     
                    
    def user_licence(self):
        # sql = "SELECT * FROM users WHERE username = %s"

        cursor = self.isConnected.cursor()
        cursor.execute(username_select, (self.username , ))
        row  = cursor.fetchall()

        print("Driving Licence : ", row[0][8])
        choice, is_valid = valid_input_user("\n[1] Add Licence Number \t[2] Back\nEnter Choice : ")
        if(choice == '1'):
            if(row[0][8] == None):
                self.add_licence()
                return True
        elif(choice == '2'): 
            return False
        else:
            return False
            print("Wrong Input")
            # self.user_licence()  



    def menu(self):
        choice_sub_menu = "\n[1] Show History\t [2] Wallet \t[3] Licence Number \t[4] Back\nEnter Choice : "
        choice_sub_menu, is_valid_input =  valid_input_user(choice_sub_menu)

        if(is_valid_input):
            choice_sub_menu = int(choice_sub_menu)
            if(choice_sub_menu == 1):
                self.showHistory()
            elif(choice_sub_menu == 2):
                self.wallet()
            elif(choice_sub_menu == 3):
                self.user_licence()    
            elif(choice_sub_menu == 4):
                return  
            else:
                print("\nWrong Number ")



     


    def pick_up_car(self):
 
       current_state  =  valid_name("\nEnter current state name :  ")
       current_city  =  valid_name(" \nEnter current city name : ")
       current_home_town  = valid_address()
       cursor = self.isConnected.cursor()
       
       # sql = "UPDATE users SET state = %s WHERE username = %s"
       cursor.execute(update_state, (current_state, self.username ))
       self.isConnected.commit()


       cursor = self.isConnected.cursor()
       # sql = "UPDATE users SET city = %s WHERE username = %s"
       cursor.execute(update_city, (current_city, self.username))
       self.isConnected.commit()

       cursor = self.isConnected.cursor()
       # sql = "UPDATE users SET city = %s WHERE username = %s"
       cursor.execute(update_home_town, (current_home_town, self.username))
       self.isConnected.commit()
       print("\nLocation Updated")

       # sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s"
       cursor = self.isConnected.cursor()
       cursor.execute(car_details_select_filter, (current_state, current_city, "YES"))
       records = cursor.fetchall()
       # print(records)
       car_id_list, car_type_id_list, choice_car_id  = [], [], []
       if(len(records) != 0):
            for row in records:
                car_id_list.append(row[4])
                car_type_id_list.append(row[5])
                choice_car_id.append(row[4])

                # print("car_id_list : --------------------------------", car_id_list)
                # print("car_type_list : ----------------", car_type_id_list)   
            for car_type_id in car_type_id_list:
                # sql = "SELECT * FROM car_type WHERE car_type_id = %s "
                cursor = self.isConnected.cursor()
                cursor.execute(car_type_id_select , (car_type_id, ))
                records = cursor.fetchall()
                if(len(records) != 0 ):
                    for row in records:
                        print("--------------------------------") 
                        print("\nCar Type Name : ", row[1])
                        car_id = car_id_list[0]
                        car_id_list.pop(0)
                        # sql = "SELECT * FROM car_name WHERE car_id = %s"
                        cursor = self.isConnected.cursor()
                        cursor.execute(car_name_id_select, (car_id, ))
                        records = cursor.fetchall()
                        if(len(records) !=0):

                            for row in records:

                               print("Car ID : ", car_id)
                               print("Car Name : ", row[1])
                               four_hour_price = row[3]
                               per_day_price = row[4]
                               # print("Four Hour Price : ",four_hour_price)
                               print("Per Day Price : ", per_day_price)



            # select_car_id  = input("\nEnter Car ID : ")
            select_car_id, is_valid  = valid_input_user("\nEnter Car ID : ")
            # print(select_car_id,  choice_car_id)

            if(int(select_car_id) in choice_car_id ):
                per_day_driver_price = 1000
                car_pick_date , date1,  = valid_date_month_year("\nEnter Year and Month and Date  for pick Up car ")
                # car_pick_date = "{0}-{1}-{2} {3}:{4}:".format(pick_date, pick_month, pick_year, pick_time, "00:00")    
                car_drop_off_date, date2 = valid_date_month_year("\nEnter Year and Month and Date  for Drop Off car ")
            
                # car_drop_off_date = "{0}/{1}/{2}-{3}:{4}".format(drop_date, drop_month, drop_year, drop_time, "00:00")
                # car_drop_off_date = "{0}-{1}-{2} {3}:{4}:".format(drop_date, drop_month, drop_year, drop_time, "00:00")

                # print(car_drop_off_date, '2022-10-5 14:00:00-08')

                # '2022-10-5 14:00:00-08'

                different = (date2 - date1).days
                if(different > 0):
                    price = different * per_day_price

                    # is_want_driver = input("\nDo you want to driver yes/no ?: ")

                    payment_succesfully, is_driver_book = False, "NO" 
                    amount = per_day_price * different 
                    if(take_decision("\nDo you want to driver yes/no ?: ")):


                       # sql = "SELECT * FROM driver WHERE state = %s and city = %s  and is_available = %s "
                       cursor = self.isConnected.cursor()
                       cursor.execute(driver_select, (current_state, current_city, "YES"))
                       records = cursor.fetchall()
                       # print(records)
                       if(len(records) !=0):
                            driver_licence = records[0][0]
                            total_driver_amount = per_day_driver_price * different
                            print("Driver's amount is : ", total_driver_amount)
                           
                            price =  amount + total_driver_amount
                            is_driver_book = "YES"
                       else:
                          # is_continue = input("Driver is not available do you want to continue of only rent car yes/no? ")
                          if(take_decision("Driver is not available do you want to continue of only rent car yes/no? ")):
                            price = amount
                            self.licence_available()
                             
                          else:
                              return   

                    else:
                        price = amount
                        self.licence_available()
                      
                    # is_pay =  input("Do you want to Pay yes/no ? ")
                    if(take_decision("Do you want to Pay yes/no ? ")):
                        # sql = "SELECT * FROM users WHERE username = %s"
                        cursor = self.isConnected.cursor()
                        cursor.execute(username_select, (self.username, ))
                        records = cursor.fetchall()
                        # print(records)
                        wallet =  records[0][5]
                        wallet = int(wallet)
                        if( price <= wallet):
                              # sql = "UPDATE users  SET wallet = %s WHERE username = %s"
                              cur = self.isConnected.cursor()
                              cur.execute(update_wallet, (int(wallet - price), self.username))
                              self.isConnected.commit()
                              payment_succesfully = True
                        else:
                             print("Add Amount in wallet ")  
                             self.wallet()
                             self.pick_up_car()   



                        if(payment_succesfully):
                           # sql = "SELECT * FROM car_details WHERE state = %s and city = %s  and is_car_available = %s and car_id = %s "
                           cursor = self.isConnected.cursor()
                           cursor.execute(car_details_filter_id, (current_state, current_city, "YES", select_car_id))
                           records = cursor.fetchall()
                           # print(records)
                           car_number = records[0][0]

                           
                           

                           userId = self.get_userId()

                           # sql = "INSERT INTO car_rent (car_number, price,  user_id, is_driver_booked, car_pick_up_date, car_drop_off_date) VALUES (%s, %s, %s, %s, %s, %s)"
                           cursor = self.isConnected.cursor()
                           details =  (car_number, price,  userId, is_driver_book, car_pick_date, car_drop_off_date)
                           cursor.execute(car_rent_insert, details)
                           self.isConnected.commit()

            
                           # sql = "UPDATE car_details  SET is_car_available = %s WHERE car_number = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(car_details_update, ("NO", car_number ))
                           self.isConnected.commit()


                           self.add_user_history(details)
                           self.add_admin_wallet(price)



                           # print("yes")
                           # return 



                           if(is_driver_book == 'YES'):
                               # sql = "SELECT * FROM driver WHERE state = %s and city = %s  and is_available = %s "
                               
                               cursor = self.isConnected.cursor()
                               cursor.execute(driver_select, (current_state, current_city, "YES"))
                               records = cursor.fetchall()

                               # print(records)
                               if(len(records) !=0):
                                   driver_licence = records[0][0]
                                   # sql = "UPDATE driver  SET is_available = %s WHERE driver_licence = %s"
                                   cursor = self.isConnected.cursor()
                                   cursor.execute(update_driver_details , ("NO", driver_licence ))
                                   self.isConnected.commit()

                                   # sql = "INSERT INTO driver_book (car_number, user_id,  driver_licence, driver_pick_up_car, driver_drop_off_date) VALUES (%s, %s, %s, %s, %s)"
                                   cursor = self.isConnected.cursor()
                                   details =  (car_number, userId,  driver_licence, car_pick_date, car_drop_off_date)
                                   cursor.execute(driver_booked_insert,  details)
                                   self.isConnected.commit()
                                   
                                   # cursor = self.isConnected.cursor()
                                   # cursor.execute(update_driving_licence_car_rent , (driver_licence, car_number ))
                                   # self.isConnected.commit()
                                   

                           else:

                                cursor = self.isConnected.cursor()
                                cursor.execute(select_user_licence, (self.username, ))
                                records = cursor.fetchall()
                                if(len(records) !=0):
                                    driver_licence = records[0][8]
                                   

                                    # cursor = self.isConnected.cursor()
                                    # cursor.execute(update_driving_licence_car_rent , (driver_licence, car_number ))
                                    # self.isConnected.commit()

                           cursor = self.isConnected.cursor()
                           cursor.execute(update_driving_licence_car_rent , (driver_licence, car_number ))
                           self.isConnected.commit() 
                            





                                   
                                   










                                   
                               # else:
                               #     print("Driver is not available")    


                           print("\nCar rent Successfully")

                else:
                     print("\nInvalid Date ")    

            else:
                print("\nInvalid select car id ")    
 
       else:
           print("\nCar is not available for current city ")
           return           



    def dash_board(self):
        if(self.isConnected != None):
            choice_interface = "\n[1] Menu\t [2] Pick Up Car \t[3] Log Out \nEnter Choice : "
            choice_interface, is_valid_input =  valid_input_user(choice_interface)
            if(is_valid_input): 
                choice_interface = int(choice_interface)
                if(choice_interface == 1):
                    self.menu()
                    self.dash_board() 
                elif(choice_interface == 2 ):
                    self.pick_up_car()
                    self.dash_board() 
                elif(choice_interface == 3):
                    print("\nSuccessfully LogOut Account \n")   
                    return
                else:
                   print("\nWrong choice ")  
                   self.dash_board()     


if __name__ == '__main__':
    dashboard = UserDashBoard('hello1')
    # dashboard.wallet()                    
    dashboard.dash_board() 

