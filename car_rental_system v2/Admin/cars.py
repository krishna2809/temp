import sys
sys.path.append("..")
from Database.database_connection import DatabaseConnection

from valid_input import valid_input_user, string_input, valid_name,  valid_mobile_number
from query import * 



class Car:


    def __init__(self):
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()


    def show_car_name(self):
        # sql = "SELECT * FROM car_name"
        cursor = self.isConnected.cursor()
        cursor.execute(car_name_select)
        records = cursor.fetchall()
        if(len(records) != 0):
              print("\n {0:-^50s}  \n ".format(""))
              for row in records:
                    print(f"Car Id : {row[0]} | \tCar Name : {row[1]} | \tTotal Car : {row[2]} | \tPer Day Price : {row[4]} | \tCar Type ID : {row[5]} ")
                    print(" {0:-^50s}  \n ".format(""))
              return True      
        
        print("\nCar  Name records isn't Available")
        return False 

    def show_car_details(self):
        # sql = "SELECT * FROM car_details"
        cursor = self.isConnected.cursor()
        cursor.execute(car_details_select )
        records = cursor.fetchall()
        if(len(records) != 0):
              print("\n {0:-^50s}  \n ".format(""))
              for row in records:
                    print(f"Car Number : {row[0]} |  \tColor : {row[1]} | \tState : {row[2]} | \tCity : {row[3]} | \tCar ID : {row[4]} | \tCar Type ID : {row[5]} \tis_car_available : {row[6]}")
                    print(" {0:-^50s}  \n ".format(""))
              return True
                    
        print("\nCar Details records isn't Available") 
        return False 



    def show_car_type(self):
        # sql = "SELECT * FROM car_type"
        cursor = self.isConnected.cursor() 
        cursor.execute(car_type_select)
        records = cursor.fetchall()
        if(len(records) != 0):
              print("\n {0:-^50s}  \n ".format(""))
              for row in records:
                    print(f"Car Type Id : {row[0]} | \tCar Type Name : {row[1]} ")
                    print(" {0:-^50s}  \n ".format(""))
              return True      

        print("\nCar Type Name records isn't Available")
        return False


    def show_car_rent(self):
        # sql = "SELECT * FROM car_rent"
        cursor = self.isConnected.cursor()
        cursor.execute(car_rent_select)
        records = cursor.fetchall()
        if(len(records) != 0):
              print("\n {0:-^50s}  \n ".format(""))
              for row in records:
                    print(f"Car Number : {row[0]} |  \tPrice : {row[1]} | \tUser Id : {row[2]} | \tIs Driver Booked : {row[3]} | \tcar Pick Up Date : {row[4]} | \tCar Drop Off Date : {row[5]} |\tDriving Licence : {row[6]} ")
                    print(" {0:-^50s}  \n ".format(""))
              return True      
        
        print("\nCar Rent records isn't Available") 
        return False


    def add_car_type(self):
       
            car_type_name, is_valid  = string_input("\nEnter Car Type Name : ")
            if(is_valid):
                # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_name = %s )"
                cursor = self.isConnected.cursor()
                cursor.execute(car_type_name_is_true, (car_type_name, ))
                row  = cursor.fetchall()
                # print("row : ", row )
                if(len(row) == 0):
                    # sql = "INSERT INTO car_type (car_type_name) VALUES (%s)"
                    cursor = self.isConnected.cursor()
                    cursor.execute(car_type_insert, (car_type_name, ))
                    self.isConnected.commit()
                    print("\nCar Type name is added successfully")
                    return True
                else:
                   print("\nCar Type name is already Available. you can't added")    
            return False 
    	        

    def add_car_name_and_price(self):
        
            car_name, is_valid  = string_input("\nEnter Car Name : ")
            if(is_valid):
    	        # sql = "SELECT * FROM car_name WHERE car_name = %s"
                # sql = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_name = %s )"

                cursor = self.isConnected.cursor()
                cursor.execute(car_name_is_true , (car_name, ))
                row  = cursor.fetchall()
                if(len(row) == 0):
                    total_car, is_valid = valid_input_user("\nEnter total Number of car : ")
                    four_hour_price, is_valid =  valid_input_user("\nEnter Four Hour price of Car :  ")
                    per_day_price, is_valid = valid_input_user("\nEnter Per day price of Car : ")
                    car_type_id, is_valid = valid_input_user("\nEnter car Type ID : ")

                    # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"

                    cursor = self.isConnected.cursor()
                    cursor.execute(car_type_is_true, (int(car_type_id), ))
                    row  = cursor.fetchall()
                    if(len(row) != 0 ):
                        # sql = "INSERT INTO car_name (car_name, total_car,  four_hour_price, per_day_price, car_type_id) VALUES (%s, %s, %s, %s, %s)"
                        cursor = self.isConnected.cursor()

                        details =  (car_name, total_car, four_hour_price, per_day_price, car_type_id)
                        cursor.execute(car_name_insert, details)
                        self.isConnected.commit()
                        print("\nCar name and price is added successfully")
                        return True
                    else:
                       print("Enter First Car Type Name ")    
                else:
                    print("\nCar name is already Available. you can't added")    
            return False 
      

    def add_car_details(self):
        
            car_number, is_valid = string_input("\nEnter Car Number : ")
            if(is_valid):
                # sql = "SELECT * FROM car_details WHERE car_number = %s"
                # sql = "SELECT * FROM car_details WHERE  EXISTS (SELECT * FROM car_details WHERE car_number = %s )"
                cursor = self.isConnected.cursor()

                cursor.execute(car_details_is_true, (car_number, ))
                row  = cursor.fetchall()
                if(len(row) == 0):
                   car_id, is_valid = valid_input_user("\nEnter car Id : ")
                   if(is_valid):
                       # sql = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_id = %s )"

                       cursor = self.isConnected.cursor()
                       cursor.execute(car_id_is_true, (int(car_id), ))
                       row  = cursor.fetchall()
                       if(len(row) != 0 ):
                              car_type_id, is_valid = valid_input_user("\nEnter Car Type Id : ")
        		              # sql = "SELECT * FROM car_type WHERE car_type_id = %s"
                              # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"

                              cursor = self.isConnected.cursor()
                              cursor.execute(car_type_id_is_true, (int(car_type_id), ))
                              row  = cursor.fetchall()
                              if(len(row) != 0 ):
                                car_color = valid_name("Enter Car Color : ")
                                state = valid_name("\nEnter State : ")
                                city = valid_name("\nEnter  City : ")
                                # sql = "INSERT INTO car_details (car_number, car_color,  state, city, car_id, car_type, is_car_available) VALUES (%s, %s, %s, %s, %s, %s, %s)"
                                cursor = self.isConnected.cursor()
                                details =  (car_number, car_color,  state, city, car_id, car_type_id, "YES")
                                cursor.execute(car_details_insert, details)
                                self.isConnected.commit()
                                print("\nCar name and price is added successfully")
                                return True
                              else:
                                  print("\nEnter First Car Type Name ")        

                       else: 
                            print("\nEnter first Car Name then and car details")	  

                else:
                        print("\nCar number already exist in records ") 
            return False


    def add_car(self):
        choice, is_valid = valid_input_user("\n[1] Add Car Type \t [2] Add Car Name and price \t[3] Add Car Details \t[4] Show Car Type \t[5] Show Car Name \t[6] Show Car Details \t[7] Back\nEnter Choice : ")
        if(is_valid):
            if(choice == '1'):
               self.add_car_type()
            elif(choice == '2'):
               self.add_car_name_and_price()
            elif(choice == '3'):
               self.add_car_details()
            elif(choice == '4'):
               self.show_car_type()
            elif(choice == '5'):
               self.show_car_name()
            elif(choice == '6'): 
                self.show_car_details() 	
            elif(choice == '7'):
                print("\nThank You ")
                return
            else:
                print("\nWrong Choice")
        self.add_car()           

            
        return False 


    def delete_car_type(self):
        print("\nShow Car Type")
        if(self.show_car_type()):
            car_type_id , is_valid= valid_input_user("\nEnter Car type Id : ")
            
            car_type_id = int(car_type_id)
         
            # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"

            cursor = self.isConnected.cursor()
            cursor.execute(car_type_id_is_true, (car_type_id, ))
            row = cursor.fetchall()
            if(len(row) != 0):
                # sql = "DELETE FROM car_type WHERE car_type_id = %s"
                cur = self.isConnected.cursor()
                cur.execute(delete_car_type, (car_type_id, ))
                self.isConnected.commit()
                print("\nSuccessfully deleted") 
            else: 
               print("\nInvalid Car Type ID ")



    def delete_car_name(self):
        print("\nShow Car Name")
        if(self.show_car_name()):
            car_id, is_valid = valid_input_user("\nEnter Car Id : ")
            if(is_valid):
                car_id = int(car_id)
                # sql = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_id = %s )"

                cursor = self.isConnected.cursor()
                cursor.execute(car_id_is_true, (car_id, ))
                row = cursor.fetchall()
                if(len(row) != 0):
                    # sql = "DELETE FROM car_name WHERE car_id = %s"
                    cur = self.isConnected.cursor()
                    cur.execute(delete_car_name, (car_id, ))
                    self.isConnected.commit()
                    print("\nSuccessfully deleted") 
                else: 
                   print("\nInvalid Car Type ID ")


    def delete_car_details(self):
        print("\nShow Car Details")
        if(self.show_car_details()):
            car_number, is_valid = string_input("\nEnter Car Number : ")
           
            # sql = "SELECT * FROM car_details WHERE  EXISTS (SELECT * FROM car_details WHERE car_number = %s )"

            cursor = self.isConnected.cursor()
            cursor.execute(car_details_is_true, (car_number,))
            row = cursor.fetchall()
            if(len(row) != 0):
                # sql = "DELETE FROM car_details WHERE car_number = %s"
                cur = self.isConnected.cursor()
                cur.execute(delete_car_detail, (car_number, ))
                self.isConnected.commit()
                print("\nSuccessfully deleted") 
            else: 
               print("\nInvalid Car Number ")

    def delete_rent_car(self):

        print("\nShow Car Rent")
        if(self.show_car_rent()):
            car_number, is_valid = string_input("\nEnter Car Number : ")
            
              
            # sql = "SELECT * FROM car_rent WHERE  EXISTS (SELECT * FROM car_rent WHERE car_number = %s )"

            cursor = self.isConnected.cursor()
            cursor.execute(car_rent_is_true, (car_number,))
            records = cursor.fetchall()
            if(len(records) != 0):
                   
                    cursor = self.isConnected.cursor()
                    cursor.execute(car_details_update,  ('YES', car_number))
                    self.isConnected.commit()


                    cursor = self.isConnected.cursor()
                    cursor.execute(select_car_rent_driver_licence, ('YES', car_number))
                    row = cursor.fetchall()
                    if(len(row) != 0):
                        driver_licence = row[0][6]
                        cursor = self.isConnected.cursor()
                        cursor.execute(update_driver_is_available,  ('YES', driver_licence))
                        self.isConnected.commit()

                    # sql = "DELETE FROM car_rent WHERE car_number = %s"
                    cur = self.isConnected.cursor()
                    cur.execute(delete_car_rent, (car_number, )) 
                    self.isConnected.commit()
                    print("\nSuccessfully deleted") 
                        









            else: 
               print("\nInvalid Car Number")
                    


    def delete_car(self):
        choice, is_valid = valid_input_user("\n[1] Delete Car Type Name  \t [2] Delete Car Name  \t[3] Delete Car Details \t[4] Delete Rent Car \t[5] Back \nEnter Choice : ")
        if(is_valid):
            if(choice == '1'):
                self.delete_car_type()
            elif(choice == '2'):
                self.delete_car_name()
            elif(choice == '3'):
                self.delete_car_details()
            elif(choice == '4'):
            	self.delete_rent_car()
            elif(choice == '5'):
                print("\nThank You")  	
                # self.operations()
                return
            else:
                print("\nWrong Choice") 
            self.delete_car()   

     


    def update_car_type(self):
        print("\nShow Car Type")
        if( self.show_car_type()):
            car_type_id, is_valid = valid_input_user("\nSelect Car Type ID : ")
            if(is_valid):
                car_type_id = int(car_type_id)
             
                # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_id = %s )"

                cursor = self.isConnected.cursor()
                cursor.execute(car_type_id_is_true, (car_type_id, ))
                row  = cursor.fetchall()
                if(len(row) != 0 ):
                    car_type_name, is_valid =  string_input("Enter Car Type Name :  ")
                    if(is_valid):
                        # sql = "SELECT * FROM car_type WHERE  EXISTS (SELECT * FROM car_type WHERE car_type_name = %s )"
                        cursor = self.isConnected.cursor()
                        cursor.execute(car_type_name_is_true, (car_type_name, ))
                        row  = cursor.fetchall()
                        if(len(row) == 0 ):
                           # sql = "UPDATE car_type  SET car_type_name = %s WHERE car_type_id = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(update_car_type, (car_type_name, car_type_id))
                           self.isConnected.commit()
                           print("\nCar Type successfully updated")
                        else:
                           print("\nCar type name is already exist ")
                else:
                    print("\nInvalid  Car type Id ") 	

     
    def update_car_name_and_price(self):
        print("\nShow car name and price")
        if(self.show_car_name()):
            car_id, is_valid = valid_input_user("\nSelect Car Id : ")
            if(is_valid):
                car_id = int(car_id)
                # sql = "SELECT * FROM car_name WHERE  EXISTS (SELECT * FROM car_name WHERE car_id = %s )"

                cursor = self.isConnected.cursor()
                cursor.execute(car_id_is_true, (car_id , ))
                row  = cursor.fetchall()
                if(len(row) != 0):
                    field_list  = ['car_name', 'total_car', 'four_hour_price', 'per_day_price']
                    print("\n---------------------")
                    for name in field_list:
                        print(name, end = '\t')
                    print("\n---------------------")

                    field_name = input("\nEnter Field Name if you want to change : ").lower()
                    if(field_name in  field_list):
                        if(field_list[0] == field_name):
                           car_name, is_valid = string_input("Enter Car name : ")
                           # sql = "SELECT * FROM car_name WHERE car_name = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(car_name_select_with_name, (car_name, ))
                           row = cursor.fetchall()
                           if(len(row) == 0 ):
                               # sql = "UPDATE car_name  SET car_name= %s WHERE car_id = %s"
                               cursor = self.isConnected.cursor()
                               cursor.execute(update_car_name, (car_name , car_id ))
                               self.isConnected.commit()
                               print("\nCar name sucessfully updated")
                           else:
                              print("\nCar name is already Available. you can't update")    



                        elif(field_list[1] == field_name):
                           total_car, is_valid = valid_input_user("Enter Total Car : ")
                           # sql = "UPDATE car_name  SET total_car = %s WHERE car_id = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(updat_total_car, (int(total_car), car_id ))
                           self.isConnected.commit()
                           print("\nState successfully updated")

                        elif(field_list[2] == field_name):
                           four_hour_price, is_valid = valid_input_user("Enter Four Hour Price : ")
                           
                           four_hour_price = int(four_hour_price)
                           # sql = "UPDATE car_name  SET four_hour_price = %s WHERE car_id = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(update_four_hour_price, (four_hour_price, car_id ))
                           self.isConnected.commit()
                           print("\nPrice is added  successfully")
                        elif(field_list[2] == field_name):
                           per_day_price, is_valid = valid_input_user("Enter per day Price : ")
                           per_day_price = int(per_day_price)
                           # sql = "UPDATE car_name  SET per_day_price = %s WHERE car_id = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(update_per_day_price , (per_day_price, car_id ))
                           self.isConnected.commit()
                           print("\nPrice is added  successfully")         
                    else:
                       print("\nInvalid Field Name ")
                else:
                   print("\nInvalid Car ID ")         	



    def update_car_details(self):
        print("\nShow Car details")
        if(self.show_car_details()):
            car_number, is_valid = string_input("\nSelect Car Number : ")
            
            
            # sql = "SELECT * FROM car_details WHERE  EXISTS (SELECT * FROM car_details WHERE car_number = %s )"

            cursor = self.isConnected.cursor()
            cursor.execute(car_details_is_true, (car_number , ))
            row  = cursor.fetchall()
            if(len(row) != 0):
                field_list  = ['car_color', 'state', 'city']
                print("\n-------------------------------")
                for name in field_list:
                    print(name, end = '\t')
                print("\n-------------------------------")
                field_name = input("Enter Field Name if you want to change : ").lower()
                if(field_name in  field_list):
                    if(field_list[0] == field_name):
                       car_color = valid_name("Enter Car Color : ")
                       if(is_valid):
                           # sql = "UPDATE car_details  SET car_color = %s WHERE car_number = %s"
                           cursor = self.isConnected.cursor()
                           cursor.execute(car_color_update, (car_color, car_number ))
                           self.isConnected.commit()
                           print("\nCar color successfully updated")


                    elif(field_list[1] == field_name):
                       state = valid_name("Enter State : ")
                       # sql = "UPDATE car_details SET state = %s WHERE car_number = %s"
                       cursor = self.isConnected.cursor()
                       cursor.execute(car_state_update, (state, car_number ))
                       self.isConnected.commit()
                       print("\nState successfully updated")

                    elif(field_list[2] == field_name):
                       city = valid_name("Enter city : ")
                      
                       # sql = "UPDATE car_details  SET city = %s WHERE car_number = %s"
                       cursor = self.isConnected.cursor()
                       cursor.execute(car_city_update, (city, car_number ))
                       self.isConnected.commit()
                       print("\nCity successfully updated")     
                else:
                   print("\nInvalid Field Name ")
            else:
                print("\nInvalid Car ID ")         	


    def update_car(self):
        choice, is_valid = valid_input_user("\n[1] Update Car Type Name  \t [2] Update Car Name and price \t[3] Update Car Details \t[4] Back \nEnter Choice : ")
        if(is_valid):
            if(choice == '1'):
                self.update_car_type()
            elif(choice == '2'):
                self.update_car_name_and_price()
            elif(choice == '3'):
                self.update_car_details()
            elif(choice == '4'):
                print("\nThank You")
                return
            else:
                print("\nWrong Choice") 
            self.update_car()    

    def operations(self):

        if(self.isConnected != None):
            isContinue = True
            while isContinue:
                choice, is_valid = valid_input_user("\n[1] Add  Car \t [2] Update Car \t[3] Delete Car \t[4] Show Car Type \t[5] Show Car Name and Price \t[6] Show Car Details \t[7] Show Rent Car \t[8] Back \nEnter Choice : ")
                if(is_valid):
                   if(choice == '1'):
                       self.add_car()
                   elif(choice == '2'):
                       self.update_car()
                   elif(choice == '3'):	
                       self.delete_car()
                   elif(choice == '4'):
                        self.show_car_type()
                   elif(choice == '5'):
                       self.show_car_name()                        
                   elif(choice == '6'):
                       self.show_car_details()
                   elif(choice == '7'):
                      self.show_car_rent()             
                   elif(choice == '8'):
                      isContinue = False     
                   else: 
                      print("\nWrong Choice")



if __name__ == "__main__":
    car =  Car()
    car.operations()