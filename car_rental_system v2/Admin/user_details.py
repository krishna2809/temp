
import sys
sys.path.append("..")

from Database.database_connection import DatabaseConnection
from valid_input import valid_input_user, valid_username
from query import  user_select,  delete_user,user_details

class UserDetails:
    def __init__(self):
        self.database_obj = DatabaseConnection()
        self.isConnected = self.database_obj.connecting()

    
    def user_delete(self):

        username = valid_username()
        cursor = self.isConnected.cursor()
        cursor.execute(user_select , (username, ))
        records = cursor.fetchall()
       
        if(len(records) != 0):
                # sql = "DELETE FROM car_rent WHERE car_number = %s"
                cur = self.isConnected.cursor()
                cur.execute(delete_user, (username, )) 
                self.isConnected.commit()
                print("\nUser is Successfully deleted") 
        else:
            print("User name is not available Or may be user's amount is available in Wallet")   


    def show_user_details(self):
        cursor = self.isConnected.cursor()
        cursor.execute(user_details)
        records = cursor.fetchall()
        print(" {0:-^50s}  \n ".format(""))
        for row in records:
        	# mobile_number |  password  | is_super_user | wallet |   city    |  state  | licence_number
            print(f"username : {row[1]}  |  Mobile Number : {row[2]}  |  Is super User : {row[4]}  |  city : {row[6]}  |  State : {row[7]}  |  Licence Number : {row[8]}")
            print(" {0:-^50s}  \n ".format(""))
     
        






    def operations(self):
        isContinue = True
        while(isContinue): 
            choice , is_valid_input =  valid_input_user("\n[1] Show User Details \t[2] Delete Users  \t[3] Back \nEnter Choice : ")
            if(is_valid_input):
                if(choice == '1'):
                    self.show_user_details()
                elif(choice == '2'):
                    self.user_delete()
               
                elif(choice == '3'):
                   print("\nThank You")
                   isContinue = False
                else: 
                   print("\nWrong Input")   
